# Clases Inteligentes

Esta es un repositorio donde se ira complementando clases inteligentes. El motivo de esto es crear clases que en lugar de establecer métodos ya fijos se pueda crear una metodología de hablarle a la clase, es decir, decirle a la clase que haga X cosa (para lo que fue programado) de una manera como si estuvieras conversando con la clase.

Hasta al momento se ha empezado con una clase para la manipulación de fechas, incluyendo el tiempo, que lo puedes ver [aquí mismo](core/fechas/readme.md)

Pero como toda sociedad debe haber reglas y esas reglas van ir dependiendo de cada clase que se vaya generando. Para que se den una idea de las reglas incorporare las reglas de la primera clase que se generó que `Dates`

Reglas a seguir
-------------
1. Como la clase es para su manipulación, deben determinar al principio que tipo de manipulación es, si es agregar, restar, establecer o extraer, para esto ocupamos nombres en inglés, así que quedaría de esta manera

    * Agregar - `ADD`
    * Restar  - `SUB`
    * Establecer - `SET`
    * Extraer - `GET`
    
1. Después de eso, seria las operaciones a seguir, puede ir más de una, pero para su formato correcto deben seguir las reglas de **CamelCase**.
1. Los argumentos van ir de acuerdo a las operaciones que se pusieron, si pones 2 operaciones, la funciona tomará los 2 argumentos, aun que pongas 3 argumentos el tercero será ignorado.
1. Pero si pones menos argumentos de las operaciones, el último argumento será tomado para las siguientes operaciones
1. Si se omite argumentos este se tomará como el valor base que es **1**
1. La clase estará tomado estilo CHAIN

Bueno esto es un ejemplo de las reglas a seguir en la clase `Dates`, aun que ahorita la clase está incompleta y se ira a agregando más funcionalidad irán aumentando las reglas a seguir que se ira explicando en su apartado.
