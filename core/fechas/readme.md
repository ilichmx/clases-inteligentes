# Clase Dates

Clase dedicada a la manipulación de fechas como agregar, restar, establecer y extraer información a las fechas.

Reglas a seguir
-------------
1. Como la clase es para su manipulación, deben determinar al principio que tipo de manipulación es, si es agregar, restar, establecer o extraer, para esto ocupamos nombres en inglés, así que quedaría de esta manera

    * Agregar - `ADD`
    * Restar  - `SUB`
    * Establecer - `SET`
    * Extraer - `GET`
    
1. Después de eso, seria las operaciones a seguir, puede ir más de una, pero para su formato correcto deben seguir las reglas de **CamelCase**.
1. Los argumentos van ir de acuerdo a las operaciones que se pusieron, si pones 2 operaciones, la funciona tomará los 2 argumentos, aun que pongas 3 argumentos el tercero será ignorado.
1. Pero si pones menos argumentos de las operaciones, el último argumento será tomado para las siguientes operaciones
1. Si se omite argumentos este se tomará como el valor base que es **1**
1. La clase estará tomado estilo CHAIN

Ejemplo de la clase
-------------

En este ejemplo le estamos diciendo que agregué 2 meses y 10 días, como pueden ver empiezo con `ADD` después `MONTH` y al final `DAY`, según las reglas primero el tipo de operación y cuantas operaciones y según los argumentos el primero corresponde a la operación Mes y después el siguiente al DIA
```PHP
use ilich\clases;

$fecha = new Dates();
$fecha->now()->addMonthDay(2, 10);
```
En el siguiente ejemplo le estoy diciendo que agregué 1 año más 42 días a la fecha actual y que me regrese DateTime (Fecha y Hora), ahora sí solo quisiera la fecha solo le diría getDate
```PHP
use ilich\clases;

$fecha = new Dates();
echo $fecha->now()->addYearDay(1, 42)->getDateTime();
```

Ahorita la clase solamente tiene operaciones básicas quiero ir metiendo más cosas por ejemplo pasarle una operación y decirle cuantas semanas hay, cuantos domingos hay en el año como `getYearSunday` o cuantos fines de semana hay `getYearWeekend` o lo mismo, pero en horas `getYearWeekendHour` 