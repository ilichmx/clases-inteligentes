<?php
	namespace core\fechas {

		use core\base\clase;

		/**
		 * @property \DateTime _obj
		 */
		class Dates extends clase {

			private $_isTime      = array('Hour', 'Minute', 'Second');
			private $_isOperation = array('Year', 'Month', 'Day', 'Hour', 'Minute', 'Second', 'Week');
			private $_isValue     = array('Time', 'Date', 'Timestamp', 'DateTime');

			public function __construct() {
				$this->_obj = new \DateTime();
			}

			public function __toString() {
				return (string) $this->_obj->getTimestamp();
			}

			public function __call($name, $arguments) {
				switch($name){
					case 'now':
						$this->_obj = new \DateTime();
					break;

					default:
						$calls = $this->_getCalls($name);
						if($calls !== null){
							$func = $calls[0];

							array_splice($calls, 0, 1);

							$donde = 0;
							foreach($calls as $indice => $call){
								$isOperation = in_array($call, $this->_isOperation);
								$isValue     = in_array($call, $this->_isValue);
								$isTime      = in_array($call, $this->_isTime);
								$numero      = count($arguments) > 0 ? $arguments[$donde] : 1;

								if($isOperation){
									if($func == 'add' || $func == 'sub'){
										$formato  = 'P' . ($isTime ? 'T' : '') . $numero . strtoupper(substr($call, 0, 1));
										$interval = new \DateInterval($formato);
										$this->_obj->{$func}($interval);
									}else if($func == 'get'){

									}
								}

								if($isValue){
									if($func == 'get'){
										if($call == 'Timestamp'){
											return $this->_obj->getTimestamp();
										}else{
											$forma = isset($arguments[0]) ? $arguments[0] : ($func == 'Time' ? 'H:i:s' : ($func == 'Date' ? 'Y-m-d' : 'Y-m-d H:i:s'));

											return $this->_obj->format($forma);
										}
									}else if($func == 'set' && $call != 'DateTime'){
										$value = null;

										if(is_numeric($arguments[0])){
											$value = array($arguments[0]);
										}else{
											$value = explode((strrpos($arguments[0], '-') !== false ? '-' : ':'), $arguments[0]);
										}

										call_user_func_array(array($this->_obj, $func . '' . $call), $value);
									}else if($func == 'set' && $call == 'DateTime'){
										$this->_obj = new \DateTime($arguments[0]);
									}
								}

								if($indice > (count($arguments) - 1)){
									$donde = count($arguments) - 1;
								}else{
									$donde = $indice + 1;
								}
							}

							if($func != 'get'){
								return $this;
							}
						}else{
							throw new \Exception('La estructura de comunicacion es invalida - ' . $name);
						}
					break;
				}

				return $this;
			}
		}
	}