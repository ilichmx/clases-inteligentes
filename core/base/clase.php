<?php
	namespace core\base{
		class clase{
			protected $_obj;
			protected function _getCalls($name){
				$value = null;

				if(strlen($name) > 3){
					$calls = preg_split('/(?<=[a-z])(?=[A-Z]) | (?<=[A-Z])(?=[A-Z][a-z])/x', $name);
					if(count($calls) > 1){
						if(in_array($calls[0], array('get', 'add', 'set', 'sub'))){
							$value = $calls;
						}
					}
				}

				return $value;
			}
		}
	}