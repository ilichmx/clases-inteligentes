<?php
	namespace {

		/**
		 * Class data
		 */
		class data {

			/**
			 * @author Manolo Navarro
			 *
			 * @param string $data
			 *
			 * @return void
			 */
			public static function clearData(&$data) {
				$data = trim($data);
				$data = stripcslashes($data);
				$data = htmlspecialchars($data);
			}

			/**
			 * @author Fernando Dorantes
			 *
			 * @param string $data
			 *
			 * @return void
			 */
			public static function xss(&$data) {
				$data = str_replace("#", "&#35;", $data);
				$data = htmlspecialchars($data, ENT_QUOTES);
				$data = str_replace("?", "&#63;", $data);
				$data = str_replace("--", "&#45;", $data);
				$data = str_replace("+", "&#43;", $data);
				$data = str_replace("*", "&#42;", $data);
				$data = str_replace("$", "&#36;", $data);
			}

			/**
			 * @author Manolo Navarro
			 * @author Fernando Dorantes
			 *
			 * @param string $data
			 *
			 * @return void
			 */
			public static function clearDataXss(&$data) {
				self::clearData($data);
				self::xss($data);
			}
		}
	}