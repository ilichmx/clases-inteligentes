<?php
	namespace {

		/**
		 * Class strings
		 */
		class strings {

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param string $string
			 * @param string $search
			 *
			 * @return bool
			 */
			public static function like($string, $search) {
				return strrpos($string, $search) !== false;
			}

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param string $string
			 *
			 * @return bool
			 */
			public static function inNumeric($string) {
				return preg_match('/\d/', $string) > 0;
			}

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param $string
			 *
			 * @return bool
			 */
			public static function isImage($string){
				return preg_match('/(\.jpg|\.png|\.bmp|\.gif)$/', strtolower($string));
			}
		}
	}