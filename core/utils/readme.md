# Utilidades

Recopilando funciones que nos pueden ser de gran ayuda, por el momento solo hay 3 clases, `STRING`, `ARRAYS` y `DATA`.

Estas clases se usan para la manipulación de datos, de cadenas y de Arrays que espero se vayan complementando con el paso del tiempo.

| Nombre de la clase | Descripción                    |
| ------------------ | ------------------------------ |
| `data`             | Manipulacion de Datos          |
| `strings`          | Manipulacion de Cadenas        |
| `arrays`           | Manipulacion de Matrices       |

## Clase Data
| Funciones     | Descripción                    |
| ------------- | ------------------------------ |
| `clearData`        | Limpia los datos         |
| `xss`   | Limpia los datos para ataques xss       |
| `clearDataXss`   | Hace ambas funciones `clearData` y `xss`       |

Ejemplos
-------------
Aquí los dueños de las funciones deben exponer sus ejemplos

## Clase Strings
| Funciones     | Descripción                    |
| ------------- | ------------------------------ |
| `like`        | Verifica si dentro de la cadena está el elemento que se busca         |
| `inNumeric`   | Verifica si dentro de la cadena hay un numero       |
| `isImage`     | Verifica si una cadena es el nombre de una imagen       |

Ejemplos
-------------
#### like
```PHP
$email  = 'fulano@mengano.com';
$nombre = 'Ilich Jesus Mascorro';

echo strings::like($email, '@') ? 'SI' : 'NO'; // -> SI
echo strings::like($nombre, '@') ? 'SI' : 'NO'; // -> NO
echo strings::like($nombre, 'ilich') ? 'SI' : 'NO'; // -> NO
echo strings::like($nombre, 'juan') ? 'SI' : 'NO'; // -> NO
```

#### inNumeric
```PHP
$cadenasin = 'fulano@mengano.com';
$cadenacon = 'Ilich Jesus Mascorro 2';

echo strings::inNumeric($cadenasin) ? 'SI' : 'NO'; // -> NO
echo strings::inNumeric($cadenacon) ? 'SI' : 'NO'; // -> SI
```

#### isImage
```PHP
$cadenacon = 'logo.png';
$cadenasin = 'Ilich Jesus Mascorro 2';

echo strings::isImage($cadenacon) ? 'SI' : 'NO'; // -> SI
echo strings::isImage($cadenasin) ? 'SI' : 'NO'; // -> NO
```

## Clase Arrays
| Funciones     | Descripción                    |
| ------------- | ------------------------------ |
| `clear`        | Limpia la matriz de espacion en blanco o segun el segundo parametro         |
| `isAssoc`   | Verifica si en la matriz las KEY son numericos o nombres (Asociaciones)       |
| `delete`   | Elimina un elemento de la matriz       |
| `getNearBy`   | Te regresa el número más cercano al valor que pasamos       |

Ejemplos
-------------
#### clear
```PHP
$url = '/directorio/otrodirectorio/';
$url = explode('/', $url);
print_r($url); // Array ( [0] => [1] => directorio [2] => otrodirectorio [3] => )
arrays::clear($url);
print_r($url); // Array ( [0] => directorio [1] => otrodirectorio )

$url = '/directorio/otrodirectorio/nolose';
$url = explode('/', $url);
print_r($url); // Array ( [0] => [1] => directorio [2] => otrodirectorio [3] => nolose )
arrays::clear($url);
print_r($url); // Array ( [0] => directorio [1] => otrodirectorio [2] => nolose )
arrays::clear($url, 'nolose');
print_r($url); // Array ( [0] => directorio [1] => otrodirectorio )
```

#### isAssoc
```PHP
$arranoes = array(1, 2, 3, 4, 5);
$arrasies = array('1' => 1, 'nolose' => 2, 3, 4);

echo arrays::isAssoc($arranoes) ? 'SI' : 'NO'; // -> NO
echo arrays::isAssoc($arrasies) ? 'SI' : 'NO'; // -> SI
```

#### delete
```PHP
$test = array('juan', 'perez', 'solo', 'mexico');
print_r($test); // Array ( [0] => juan [1] => perez [2] => solo [3] => mexico ) 
arrays::delete($test, 'solo');
print_r($test); // Array ( [0] => juan [1] => perez [2] => mexico ) 
```

#### getNearBy
```PHP
$test = array(1, 2, 3, 4, 7, 8, 9, 10, 15, 28, 30);
echo arrays::getNearBy($test, 5); // 4
echo arrays::getNearBy($test, 6); // 7
echo arrays::getNearBy($test, 11); // 10
echo arrays::getNearBy($test, 12); // 10
echo arrays::getNearBy($test, 13); // 15
echo arrays::getNearBy($test, 14); // 15
echo arrays::getNearBy($test, 26); // 28
echo arrays::getNearBy($test, 99); // 30
```