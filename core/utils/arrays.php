<?php
	namespace {

		/**
		 * Class arrays
		 */
		class arrays {

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param array  $arrays
			 * @param string $sign
			 *
			 * @return void
			 */
			public static function clear(&$arrays, $sign = '') {
				$arrays = array_values(array_diff($arrays, array($sign)));
			}

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param array $arrays
			 *
			 * @return bool
			 */
			public static function isAssoc($arrays) {
				return (bool) count(array_filter(array_keys($arrays), 'is_string'));
			}

			/**
			 * @author Ilich Jesus Mascorro Barrera
			 *
			 * @param array $arrays
			 * @param mixed $value
			 *
			 * @return void
			 */
			public static function delete(&$arrays, $value) {
				$index = array_search($value, $arrays);

				if($index !== false){
					unset($arrays[$index]);
				}

				$arrays = array_values($arrays);
			}

			/**
			 * @author       Ilich Jesus Mascorro Barrera
			 *
			 * @param array $arrays
			 * @param int   $value
			 *
			 * @return null || int
			 */
			public static function getNearBy(&$arrays, $value) {
				$closest = null;

				foreach($arrays as $item){
					if($closest === null || abs($value - $closest) > abs($item - $value)){
						$closest = $item;
					}
				}

				return $closest;
			}
		}
	}